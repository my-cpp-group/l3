#ifndef HW_L3_DOMAIN_LAYER_H
#define HW_L3_DOMAIN_LAYER_H

#include "hw/l4_InfrastructureLayer.h"


using namespace std;

class Squad:public ICollectable {
public:
	enum Fraction {
		MAGISTER,
		PALADIN,
		GODWOKEN

	};
	enum FightType {
		MELEE,
		RANGED
	};
	Fraction fraction;
	int strength;
	int defence;
	int health;
	int dexterity;
	int dodging;
	FightType fightType;

	bool invariant();
	
	Squad(Squad::Fraction f, int strength, int defence, int health, int dexterity, int dodging, Squad::FightType ft);

	Squad(int f, int strength, int defence, int health, int dexterity, int dodging, int ft);
	Squad() {

	};

	virtual bool write(std::ostream& os) override;

	void print() override;

    ~Squad() {};
};

class ItemCollector: public ACollector
{
public:
    virtual shared_ptr<ICollectable> read(istream& is, bool & flag) override;
};

#endif // HW_L3_DOMAIN_LAYER_H
