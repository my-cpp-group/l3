#include "hw/l3_DomainLayer.h"

using namespace std;


bool Squad::invariant()
{
    bool result= (0 <= this->strength && this->strength <= 100) && (0 <= this->dexterity && this->dexterity <= 100) && (0 <= this->defence && this->defence <= 100) &&(0 <= this->dodging && this->dodging <= 100) && (0 <= this->health && this->health <= 100);
		return result;
}

Squad::Squad(Squad::Fraction f, int strength, int defence, int health, int dexterity, int dodging, Squad::FightType ft) {
		this->fraction = f;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		this->fightType = ft;
		assert(invariant());
	};

Squad::Squad(int f, int strength, int defence, int health, int dexterity, int dodging, int ft) {
		Fraction frac;
		switch (f)
		{
		case 0: frac = Fraction::MAGISTER;
			break;
		case 1: frac = Fraction::PALADIN;
			break;
		case 2: frac = Fraction::GODWOKEN;
			break;
		default:
			frac = Fraction::GODWOKEN;
			break;
		}
		this->fraction = frac;
		this->strength = strength;
		this->defence = defence;
		this->health = health;
		this->dexterity = dexterity;
		this->dodging = dodging;
		FightType Ft;
		switch (ft)
		{
		case 0: Ft = FightType::MELEE;
			break;
		case 1: Ft = FightType::RANGED;
			break;
		default:  Ft = FightType::MELEE;
			break;
		}
		this->fightType = Ft;
		assert(invariant());
	};

void Squad::print() {
		cout << "-------------------------------------------------------------------" << endl;
		switch (this->fraction)
		{
		case Squad::Fraction::GODWOKEN:
			cout << "Fraction: Godwoken" << endl;
			break;
		case Squad::Fraction::MAGISTER:
			cout << "Fraction: Magister" << endl;
			break;
		case Squad::Fraction::PALADIN:
			cout << "Fraction: Paladin" << endl;
			break;
		}

		cout << "Strength: " << this->strength << endl << "Defence: " << this->defence << endl <<
			"Haelth: " << this->health << endl << "Dexterity: " << this->dexterity << endl << "Dodging: " << this->dodging << endl;
		if (this->fightType == Squad::FightType::MELEE) {
			cout << "Fight Type: Melee" << endl;
		}
		else {
			cout << "Fight Type: Ranged" << endl;
		}
		cout << "-------------------------------------------------------------------" << endl;
	};

bool   Squad::write(ostream& os)
{
    Squad temp(this->fraction, this->strength, this->defence, this->health, this->dexterity, this->dodging, this->fightType);
		os.write((char*)&temp, sizeof(Squad));
		return true;

}



shared_ptr<ICollectable> ItemCollector::read(istream& is, bool & flag)
{
    Squad result = readItem<Squad>(is, flag);
		return make_shared<Squad>(result);
}
