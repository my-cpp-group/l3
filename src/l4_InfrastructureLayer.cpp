#include "hw/l4_InfrastructureLayer.h"

using namespace std;




shared_ptr<ICollectable> ACollector::getItem(size_t index) const
{
    assert(index < _items.size());

    return _items[index];
}

bool ACollector::isRemoved(size_t index) const
{
    assert(index < _removed_signs.size());

    return _removed_signs[index];
}

void ACollector::printCollection() {
	for (size_t i = 0; i < _items.size(); i++)
		{
			if (!_removed_signs[i]) {
				_items[i]->print();
			}	
		}
	}

void ACollector::addItem(shared_ptr<ICollectable> item)
{
    _items.emplace_back(item);
    _removed_signs.emplace_back(false);
}

void ACollector::removeItem(size_t index)
{
    assert(index < _items.size());
    assert(index < _removed_signs.size());

    if (!_removed_signs[index])
    {
        _removed_signs[index] = true;
        _removed_count ++;
    }
}

void ACollector::updateItem(size_t index, const shared_ptr<ICollectable> & item)
{
    assert(index < _items.size());

    _items[index] = item;
}

void ACollector::clean()
{
    _items.clear();
	_removed_signs.clear();
	_removed_count = 0;
}

bool ACollector::loadCollection(const string file_name)
{
    std::ifstream fin;
	fin.open(file_name);
	if (fin.is_open())
	{
		std::cout << "The file is open" << std::endl;
		bool flag = true;
		while (flag) {
			auto temp = read(fin, flag);
			if (flag)
			{
				addItem(temp);
			}
		}
		fin.close();
		assert(invariant());
		return true;
	}
	else {
		std::cout << "The file is not open" << std::endl;
		return false;
	}
}

bool ACollector::saveCollection(const string file_name) const
{
    assert(invariant());
	std::ofstream fout;
	fout.open(file_name);
	if (fout.is_open())
	{
		std::cout << "The file is open" << std::endl;
		for (size_t i = 0; i < _items.size(); ++i)
		{
			if (!_removed_signs[i])
				_items[i]->write(fout);

		}
		fout.close();
	}
	else {
		std::cout << "The file is not open" << std::endl;
	}
	return fout.good();
}
